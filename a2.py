import os
import zipfile
import numpy as np
from bs4 import BeautifulSoup
import csv

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
import joblib
import pdfrw
import networkx as nx

from pdfalyzer.pdfalyzer import Pdfalyzer
from anytree import Node, RenderTree, Walker, PreOrderIter
from anytree.exporter import DotExporter
from anytree.util import commonancestors
from anytree.search import findall


folder_path = '/Users/sascharolinger/dev/datalab/datalab-unit3/ch02-train'

def load_data(folder_path):
    file_paths = []
    labels = []
    filenames = []
    text = []
    file_list = sorted(os.listdir(folder_path))
    for filename in file_list:
        with open(os.path.join(folder_path, filename), 'r', encoding='latin-1') as file:
            content = file.read()
            text.append(content)
        label = int(filename[-1])
        file_paths.append(os.path.join(folder_path, filename))
        labels.append(label)
        filenames.append(filename)
        
    return text, file_paths, labels, filenames

def extract_pdf_features(file_path):
    try:
       
        pdfalyzer = Pdfalyzer(file_path)
        root = pdfalyzer.pdf_tree

        nodes = list(PreOrderIter(root))
        num_nodes = len(nodes)
        leaves = [node for node in nodes if node.is_leaf]
        num_leaves = len(leaves)
        depths = [node.depth for node in nodes]
        depth = max(depths) if depths else 0
        degrees = [len(node.children) for node in nodes]
        avg_degree = np.mean(degrees)
        avg_children = np.mean(degrees)
        median_children = np.median(degrees)
        var_children = np.var(degrees)

        # Use NetworkX for additional graph metrics
        G = nx.DiGraph()
        for node in nodes:
            for child in node.children:
                G.add_edge(node.idnum, child.idnum)

        if len(G) > 1:
            degree_assortativity = nx.degree_assortativity_coefficient(G)
            # Compute the average shortest path length for each component and then average these lengths
        else:
            degree_assortativity = 0
        avg_clustering = nx.average_clustering(G.to_undirected())
        density = nx.density(G)


        return [avg_children, median_children, var_children, num_leaves, num_nodes, depth, avg_degree,
                degree_assortativity, avg_clustering, density]
    except Exception as e:
        print(f"Error extracting PDF features: {e}")
        return [0] * 10

def load_or_train_model(model_file, vect_file):
    if os.path.exists(model_file) and os.path.exists(vect_file):
        clf = joblib.load(model_file)
        vectorizer = joblib.load(vect_file)
    else:
        # Example paths to .docx files and their labels
        text, file_paths, labels, filenames = load_data(folder_path)
        # Extract features from all files
        features = []
        text_contents = []
        valid_labels = []

        for i, file_path in enumerate(file_paths):
            if i > 100: break # just for debuggin training process
            feature = extract_pdf_features(file_path)
            if feature is not None:
                features.append(feature)
                text_contents.append(text[i])
                valid_labels.append(labels[i])

        print("read everything")
        # Convert to numpy arrays
        features = np.array(features)
        labels = np.array(valid_labels)

        # Generate n-gram features using CountVectorizer
        vectorizer = CountVectorizer(ngram_range=(1, 2), max_features=1000)  # Adjust n-gram range and max_features as needed
        ngram_features = vectorizer.fit_transform(text_contents).toarray()

        # Combine all features
        combined_features = np.hstack((features, ngram_features))

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(combined_features, labels, test_size=0.3, random_state=42)

        # Train a RandomForestClassifier
        clf = RandomForestClassifier(n_estimators=100, random_state=42)
        print("start fitting")
        clf.fit(X_train, y_train)

        # Make predictions
        y_pred = clf.predict(X_test)

        # Evaluate the model
        print("Accuracy:", accuracy_score(y_test, y_pred))
        print("Classification Report:\n", classification_report(y_test, y_pred))
        joblib.dump(clf, model_file)
        joblib.dump(vectorizer, vect_file)
    return clf, vectorizer

# classify
test_path = "/Users/sascharolinger/dev/datalab/datalab-unit3/ch02-test"
output_file = "output1_final.csv"

# Extract features from all files
file_names_test = []
features_test = []
text_contents_test = []

model_file = "model2_final.pkl"
vect_file = "vect2_final.pkl"        
clf, vectorizer = load_or_train_model(model_file, vect_file)

print("read everything")

for i, file_path in enumerate(sorted(os.listdir(test_path))):
    full_path = os.path.join(test_path, file_path)
    file_names_test.append(file_path)
    feature = extract_pdf_features(full_path)
    if feature is not None:
        features_test.append(feature[:-1])
        text_contents_test.append(feature[-1])
        

# Convert to numpy arrays
features_test = np.array(features_test)
ngram_features_test = vectorizer.transform(text_contents_test).toarray()

# Combine all features
combined_features = np.hstack((features_test, ngram_features_test))

print("starting test prediction")
with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        labels = clf.predict(combined_features)
        
        for i in range(0, len(file_names_test)):
            csvwriter.writerow([file_names_test[i], labels[i]])
