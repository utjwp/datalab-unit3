import os
import zipfile
import numpy as np
from bs4 import BeautifulSoup
import csv

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
import joblib
import pdfrw
import networkx as nx
import re

from pdfalyzer.pdfalyzer import Pdfalyzer
from anytree import Node, RenderTree, Walker, PreOrderIter
from anytree.exporter import DotExporter
from anytree.util import commonancestors
from anytree.search import findall
import subprocess
import pdfid
import optparse

pdfid_script_path = 'pdfid.py'


folder_path = '/Users/sascharolinger/dev/datalab/datalab-unit3/ch02-train'

def load_data(folder_path):
    file_paths = []
    labels = []
    filenames = []
    text = []
    file_list = sorted(os.listdir(folder_path))
    for filename in file_list:
        with open(os.path.join(folder_path, filename), 'r', encoding='latin-1') as file:
            content = file.read()
            text.append(content)
        label = int(filename[-1])
        file_paths.append(os.path.join(folder_path, filename))
        labels.append(label)
        filenames.append(filename)
        
    return text, file_paths, labels, filenames

def extract_pdf_features(file_path):
    try:
        # result = subprocess.run(['python', pdfid_script_path, file_path], capture_output=True, text=True)
        #pdfid_output = result.stdout
        options = optparse.Values({
        'scan': False,
        'all': False,
        'extra': False,
        'force': False,
        'disarm': False,
        'plugins': '',
        'csv': False,
        'minimumscore': 0.0,
        'verbose': False,
        'select': '',
        'nozero': False,
        'output': '',
        'pluginoptions': '',
        'literalfilenames': True,
        'recursedir': False
    })

        pdfid_output = pdfid.PDFiDMain([file_path], options)
        pattern = r'\d+$'

        # Parse the PDFiD output
        features = []
        for line in pdfid_output.splitlines():
            line = line.strip()
            if line.startswith('obj') or line.startswith('endobj') or line.startswith('stream') or \
                line.startswith('endstream') or line.startswith('xref') or line.startswith('trailer') or \
                line.startswith('startxref') or line.startswith('/Page') or line.startswith('/Encrypt') or \
                line.startswith('/ObjStm') or line.startswith('/JS') or line.startswith('/JavaScript') or \
                line.startswith('/AA') or line.startswith('/OpenAction') or line.startswith('/AcroForm') or \
                line.startswith('/JBIG2Decode') or line.startswith('/RichMedia') or line.startswith('/Launch') or \
                line.startswith('/EmbeddedFile') or line.startswith('/XFA') or line.startswith('/Colors > 2^24') or \
                line.startswith('/Cups'):
                match = re.search(pattern, line)
                if match:
                    number = match.group()
                    features.append(int(number))
        assert len(features) == 21
        return features
    except Exception as e:
        print(f"Error extracting PDF features from {file_path}: {e}")
        print(pdfid_output)
        print(len(features))
        return [0] * 21  # Return a default feature vector of the same length

def read_features_from_csv(csv_file):
    features = []
    if os.path.exists(csv_file):
        with open(csv_file, 'r') as csvfile:
            csvreader = csv.reader(csvfile)
            next(csvreader)  # Skip the header
            for row in csvreader:
                features.append([int(x) for x in row[1:]])  # Skip the filename column
    return features

def load_or_train_model(model_file, vect_file):
    if os.path.exists(model_file) and os.path.exists(vect_file):
        clf = joblib.load(model_file)
        vectorizer = joblib.load(vect_file)
    else:
        # Example paths to .docx files and their labels
        text, file_paths, labels, filenames = load_data(folder_path)
        # Extract features from all files
        features = []
        text_contents = []
        valid_labels = []
        csv_file = "train_features.csv"
        if os.path.exists(csv_file):
            text_contents = text
            features = read_features_from_csv(csv_file)
            valid_labels = labels

        else:
            with open(csv_file, 'w', newline='') as csvfile:
                    csvwriter = csv.writer(csvfile)
                    csvwriter.writerow(['filename'] + [f'feature_{i}' for i in range(21)])  # Write header

                    for i, file_path in enumerate(file_paths):
                            feature = extract_pdf_features(file_path)
                            features.append(feature)
                            text_contents.append(text[i])
                            valid_labels.append(labels[i])
                            csvwriter.writerow([file_path] + feature)

        print("read everything")
        # Convert to numpy arrays

        features = np.array(features)
        print("converted features")
        labels = np.array(valid_labels)
        print("converted labels")

        # Generate n-gram features using CountVectorizer
        vectorizer = HashingVectorizer(analyzer='char', ngram_range=(1,1))  # Adjust n-gram range and max_features as needed
        print("vectorizer")


        
        #ngram_features = vectorizer.fit_transform(text_contents)
        print("2array")
       # ngram_features = ngram_features.toarray()

        print("fit_transformed")

        # Combine all features
        #combined_features = np.hstack((features, ngram_features))
        #combined_features = np.hstack((features))

        print("stacked")
        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.3, random_state=42)

        # Train a RandomForestClassifier
        clf = RandomForestClassifier(n_estimators=100, random_state=42)
        print("start fitting")
        print("trained")
        clf.fit(X_train, y_train)

        # Make predictions
        y_pred = clf.predict(X_test)

        # Evaluate the model
        print("Accuracy:", accuracy_score(y_test, y_pred))
        print("Classification Report:\n", classification_report(y_test, y_pred))
        joblib.dump(clf, model_file)
        joblib.dump(vectorizer, vect_file)
    return clf, vectorizer

# classify
test_path = "/Users/sascharolinger/dev/datalab/datalab-unit3/ch02-test"
output_file = "output2_final.csv"

# Extract features from all files
file_names_test = []
features_test = []
text_contents_test = []

model_file = "model2_final.pkl"
vect_file = "vect2_final.pkl"        
clf, vectorizer = load_or_train_model(model_file, vect_file)

print("read everything")

for i, file_path in enumerate(sorted(os.listdir(test_path))):
    full_path = os.path.join(test_path, file_path)
    file_names_test.append(file_path)
    feature = extract_pdf_features(full_path)
    if feature is not None:
        features_test.append(feature)
        #text_contents_test.append(feature[-1])
        

  # Convert to numpy arrays
features_test = np.array(features_test)
# ngram_features_test = vectorizer.transform(text_contents_test).toarray()

# # Combine all features


print("starting test prediction")
with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        labels = clf.predict(features_test)
        
        for i in range(0, len(file_names_test)):
            csvwriter.writerow([file_names_test[i], labels[i]])
