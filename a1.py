import os
import zipfile
import numpy as np
from bs4 import BeautifulSoup
import csv

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
import joblib


folder_path = '/Users/sascharolinger/dev/datalab/datalab-unit3/ch01-train'

def load_data(folder_path, encoding='utf-8'):
    file_paths = []
    labels = []
    filenames = []
    file_list = sorted(os.listdir(folder_path))
    for filename in file_list:
        label = int(filename[-1]) 
        with open(os.path.join(folder_path, filename), 'r', encoding=encoding) as file:
            content = file.read() 
        file_paths.append(os.path.join(folder_path, filename))
        labels.append(label)
        filenames.append(filename)
    return file_paths, labels, filenames

# Function to extract text content from the XML within a .docx file
def extract_text_from_xml(xml_content):
    try:
        soup = BeautifulSoup(xml_content, 'xml')
        text_elements = soup.find_all(['w:t', 'w:tab', 'w:br'])
        texts = [element.text for element in text_elements]
        return ' '.join(texts)
    except Exception as e:
        print(f"Error extracting text from XML: {e}")
        return ""

# Function to extract XML content from a .docx file using BeautifulSoup
def extract_xml_features(file_path):
    try:
        with zipfile.ZipFile(file_path, 'r') as docx:
            xml_content = docx.read('word/document.xml')
            soup = BeautifulSoup(xml_content, 'xml')

            # Extract features from the XML content
            valid = 1
            element_count = len(soup.find_all())
            num_images = len(soup.find_all('w:drawing'))
            num_hyperlinks = len(soup.find_all('w:hyperlink'))
            num_paragraphs = len(soup.find_all('w:p'))
            num_tables = len(soup.find_all('w:tbl'))
            fonts = {font['w:ascii'] for font in soup.find_all('w:rFonts') if font.has_attr('w:ascii')}
            num_fonts = len(fonts)

            # Extract text content for n-gram analysis
            text_content = extract_text_from_xml(xml_content)

            return [valid, element_count, num_images, num_hyperlinks, num_paragraphs, num_tables, num_fonts, text_content]
    except Exception as e:
        valid = 0
        return [valid, 0, 0, 0, 0, 0, 0, "empty"]

def load_or_train_model(model_file, vect_file):
    if os.path.exists(model_file) and os.path.exists(vect_file):
        clf = joblib.load(model_file)
        vectorizer = joblib.load(vect_file)
    else:
        # Example paths to .docx files and their labels
        file_paths, labels, filenames = load_data(folder_path, encoding='latin-1')
        # Extract features from all files
        features = []
        text_contents = []
        valid_labels = []

        for i, file_path in enumerate(file_paths):
            #if i > 100: break # just for debuggin training process
            feature = extract_xml_features(file_path)
            if feature is not None:
                features.append(feature[:-1])
                text_contents.append(feature[-1])
                valid_labels.append(labels[i])

        print("read everything")
        # Convert to numpy arrays
        features = np.array(features)
        labels = np.array(valid_labels)

        # Generate n-gram features using CountVectorizer
        vectorizer = TfidfVectorizer(analyzer='char', ngram_range=(1, 2), max_features=1000)  # Adjust n-gram range and max_features as needed
        ngram_features = vectorizer.fit_transform(text_contents).toarray()

        # Combine all features
        combined_features = np.hstack((features, ngram_features))

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(combined_features, labels, test_size=0.3, random_state=42)

        # Train a RandomForestClassifier
        clf = RandomForestClassifier(n_estimators=100, random_state=42)
        print("start fitting")
        clf.fit(X_train, y_train)

        # Make predictions
        y_pred = clf.predict(X_test)

        # Evaluate the model
        print("Accuracy:", accuracy_score(y_test, y_pred))
        print("Classification Report:\n", classification_report(y_test, y_pred))
        joblib.dump(clf, model_file)
        joblib.dump(vectorizer, vect_file)
    return clf, vectorizer

# classify
test_path = "/Users/sascharolinger/dev/datalab/datalab-unit3/ch01-test"
output_file = "output1_final.csv"

# Extract features from all files
file_names_test = []
features_test = []
text_contents_test = []


for i, file_path in enumerate(sorted(os.listdir(test_path))):
    full_path = os.path.join(test_path, file_path)
    file_names_test.append(file_path)
    feature = extract_xml_features(full_path)
    if feature is not None:
        features_test.append(feature[:-1])
        text_contents_test.append(feature[-1])
        
model_file = "model1_final.pkl"
vect_file = "vect1_final.pkl"        
clf, vectorizer = load_or_train_model(model_file, vect_file)

print("read everything")
# Convert to numpy arrays
features_test = np.array(features_test)
ngram_features_test = vectorizer.transform(text_contents_test).toarray()

# Combine all features
combined_features = np.hstack((features_test, ngram_features_test))

print("starting test prediction")
with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        labels = clf.predict(combined_features)
        
        for i in range(0, len(file_names_test)):
            csvwriter.writerow([file_names_test[i], labels[i]])
